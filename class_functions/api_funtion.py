from flask import request,jsonify,session
import json
from datetime import date,datetime
# from twilio.rest import Client
from class_functions.otp_genetate import value
from config import conn

# otp =GENERATE()
class ApiData:
    def __init__(self):
        # self.otp = GENERATE()
        pass

    def sign_in(self):
        try:
            # return jsonify({'msg':'value is true'})
            if request.method == 'POST':
                mobile = request.form.get('mobile')
                if mobile:
                    if len(mobile)==10:
                        
                        conn.cur.execute(f"select id,fullname,mobile,gender,dob from users where mobile = {mobile}")
                        user=conn.cur.fetchone()
                        
                        # print(user[2])
                        if int(mobile) == int(user[2]):
                            # print(mobile)
                            # otp_no=value.otp_generate()
                            otp_no = 1234
                            # print(otp_no)
                            sql =f"insert into otp_value(type,id_user,mobilenumber,otp,created_at) values('sign_in',{user[0]},{user[2]},{otp_no},'{datetime(2020, 5, 17)}')"
                            print(sql)
                            conn.cur.execute(sql)
                            conn.dp.commit()
                            data = {
                                'result':True,
                                'msg':'OTP send your mobile number',
                                'data':[mobile]
                            }
                            session['otp'] = otp_no
                            session['mobile'] = mobile
                            datas=json.dumps(data)
                            return datas 
                        else:
                            data = {
                                'result':False,
                                'msg':'go to sign up page',
                            }
                            datas=json.dumps(data)
                            return datas
                    else:
                        data = {
                            'result':False,
                            'msg':'kindly give me a mobile number properly'
                        }
                        datas=json.dumps(data)
                        return datas
                else:
                    data = {
                            'result':False,
                            'msg':'mobile number requied'
                        }
                    datas=json.dumps(data)
                    return datas
            else:
                    data = {
                        'result':False,
                        'msg':'use post method'
                    }
                    datas=json.dumps(data)
                    return datas
        except Exception as Ex:
            return jsonify({'msg':f'{Ex}'})


    def sign_up(self):
        try:
            if request.method == 'POST':
                    mobile = request.form.get('mobile')
                    if mobile:
                        if len(mobile)==10:
                            user = 8668109712
                            if int(mobile) != user:
                                # conn.cur.execute(f"insert into ")
                                # user=conn.cur.fetchone()
                                otp_no = 1234
                                data = {
                                    'result':True,
                                    'msg':'OTP send your mobile number',
                                    'data':[mobile]
                                }
                                session['otp'] = otp_no
                                session['mobile'] = mobile
                                datas=json.dumps(data)
                                return datas
                            else:
                                data = {
                                        'result':False,
                                        'msg':'go to the sign in page ',
                    
                                    }
                                datas=json.dumps(data)
                                return datas
                        else:
                            data = {
                                    'result':False,
                                    'msg':'kindly give me a mobile number properly',
                                    
                                }
                            datas=json.dumps(data)
                            return datas
                    else:
                        data = {
                                    'result':False,
                                    'msg':'mobile number requied',
                                    
                                }
                        datas=json.dumps(data)
                        return datas
            else:
                    data = {
                        'result':False,
                        'msg':'use post method'
                    }
                    datas=json.dumps(data)
                    return datas
        except Exception as Ex:
            return jsonify({'msg':f'{Ex}'})      
    

    def otp_page(self):
        try:
            if request.method == 'POST':
                otp = request.form.get('otp')
                data_otp = session.get('otp')
                if data_otp == '':
                    data = {
                            'result':False,
                            'msg':'otp invalid!'
                        }
                    datas = json.dumps(data)
                    return datas

                if otp:
                    if int(otp) == int(data_otp):
                        key =value.otp_generate()
                        data = {
                            'result':True,
                            'msg':'login successfully',
                            'key':key
                        }
                        session['otp'] = ''
                        session['key'] = key
                        datas = json.dumps(data)
                        return datas
                    else:
                        data = {
                            'result':False,
                            'msg':'login Failed! kindly enter valid otp'
                        }
                        datas = json.dumps(data)
                        return datas
                
                else:
                    data = {
                            'result':False,
                            'msg':'not give otp'
                        }
                    datas = json.dumps(data)
                    return datas
            else:
                data = {
                        'result':False,
                        'msg':'use post method'
                    }
                datas = json.dumps(data)
                return datas
        except Exception as Ex:
            return jsonify({'msg':f'{Ex}'})  

    def home_page(self):
        try:
            if request.method =='POST':
                mobile = request.form.get('mobile')
                key =request.form.get('key')
                sesskey =session.get('key')
                sessmobile = session.get('mobile')
                today = str(date.today())
                # print(sesskey,key,sessmobile,mobile)
                if mobile and key:
                    if mobile == str(sessmobile) and key == str(sesskey):

                        data = {
                                'result':True,
                                'username':'satheesh kumar',
                                'mobile':'8668109712',
                                'points':'150',
                                'date':today,
                                'activity':{
                                    'steps':'560',
                                    'sleep':'12Hrs 20mins',
                                    'water':'5',
                                    'swimming':'10'
                                },
                                'msg':''
                            }
                        datas = json.dumps(data)
                        return datas
                    else:
                        data = {
                                'result':False,
                                'msg':'mobile number or key invalid!'
                            }
                        datas = json.dumps(data)
                        return datas
                else:
                        data = {
                                'result':False,
                                'msg':'missing mobile number or key'
                            }
                        datas = json.dumps(data)
                        return datas
            else:
                data = {
                        'result':False,
                        'msg':'use GET method'
                    }
                datas = json.dumps(data)
                return datas
        except Exception as Ex:
            return jsonify({'msg':f'{Ex}'})
        
    def activity_page(self):
        try:
            if request.method == 'POST':
                mobile = request.form.get('mobile')
                key =request.form.get('key')
                act_type = request.form.get('type')
                sesskey =session.get('key')
                sessmobile = session.get('mobile')
                today = str(date.today())
                if mobile and key:
                    if mobile == str(sessmobile) and key == str(sesskey):
                        if act_type == 'step':
                            data = {
                                'result':True,
                                'username':'satheesh kumar',
                                'points':'150',
                                'date':today,
                                'type':'step',
                                'activity':{
                                    'step_count':'1890',
                                    'Goal':'3000',
                                    'Duration':'29 M 21 S',
                                    'Distance':'1 KM',
                                    'Calories':'73 KCAL',
                                },
                                'msg':''
                            }
                            datas = json.dumps(data)
                            return datas
                        elif act_type == 'sleep':
                            data = {
                                'result':True,
                                'username':'satheesh kumar',
                                'points':'150',
                                'date':today,
                                'type':'sleep',
                                'activity':{
                                    
                                },
                                'msg':''
                            }
                            datas = json.dumps(data)
                            return datas
                        
                        elif act_type == 'swimming':
                            data = {
                                'result':True,
                                'username':'satheesh kumar',
                                'points':'150',
                                'date':today,
                                'type':'swimming',
                                'activity':{
                                    
                                },
                                'msg':''
                            }
                            datas = json.dumps(data)
                            return datas
                        
                        elif act_type == 'water':
                            data = {
                                'result':True,
                                'username':'satheesh kumar',
                                'points':'150',
                                'date':today,
                                'type':'water',
                                'activity':{
                                    
                                },
                                'msg':''
                            }
                            datas = json.dumps(data)
                            return datas
                        
                        else:
                            data = {
                                'result':False,
                                'username':'satheesh kumar',
                                'points':'150',
                                'date':today,
                                'type':'null',
                                'activity':{
                                    
                                }
                            }
                            datas = json.dumps(data)
                            return datas



                    else:
                        data = {
                                'result':False,
                                'msg':'mobile number or key invalid!'
                            }
                        datas = json.dumps(data)
                        return datas
                else:
                        data = {
                                'result':False,
                                'msg':'missing mobile number or key'
                            }
                        datas = json.dumps(data)
                        return datas
            else:
                data = {
                        'result':False,
                        'msg':'use post method'
                    }
                datas = json.dumps(data)
                return datas
                        
        except Exception as Ex:
            return jsonify({'msg':f'{Ex}'})
    


    def profile(self):
        try:
            if request.method == 'POST':
                mobile = request.form.get('mobile')
                key =request.form.get('key')
                sesskey =session.get('key')
                sessmobile = session.get('mobile')
                if mobile and key:
                    if mobile == str(sessmobile) and key == str(sesskey):
                        user = 8668109712
                        if mobile == str(user):
                            data = {
                                'result':True,
                                'name':'satheesh kumar',
                                'DOB':'27/05/1993',
                                'Height':'5.7 Feet',
                                'Weight':'75 Kg',
                                'Gender':'Male',
                                'image':'https://sslimages.shoppersstop.com/sys-master/images/h45/heb/30626495954974/SS2316088615URL_PURPLE_alt2.jpg_2000Wx3000H'
                                
                            }
                            datas = json.dumps(data)
                            return datas
                        
                        else:
                            data = {
                                'result':False,
                                'name':'',
                                'DOB':'',
                                'Height':'',
                                'Weight':'',
                                'Gender':'',
                                'image':''
                                
                            }
                            datas = json.dumps(data)
                            return datas
                        
                    else:
                        data = {
                                'result':False,
                                'msg':'mobile number or key invalid!'
                            }
                        datas = json.dumps(data)
                        return datas
                else:
                        data = {
                                'result':False,
                                'msg':'missing mobile number or key'
                            }
                        datas = json.dumps(data)
                        return datas
            else:
                data = {
                        'result':False,
                        'msg':'use post method'
                    }
                datas = json.dumps(data)
                return datas
     
        except Exception as Ex:
            return jsonify({'msg':f'{Ex}'})
