create table users(
ID int auto_increment,
fullname varchar(255) default 0,
email varchar(255) default 0,
mobile varchar(255) default 0 unique,
dob datetime ,
gender varchar(100) default null,
status int comment "1-active and 2-inactive",
create_date datetime,
update_date datetime,
primary key(ID));


