CREATE TABLE `otp_value` (
  `id` int NOT NULL AUTO_INCREMENT,
  `uuid` varchar(50) DEFAULT NULL unique,
  `type` varchar(100) NOT NULL,
  `id_user` int DEFAULT NULL,
  `mobilenumber` varchar(25) NOT NULL,
  `otp` varchar(6) NOT NULL,
  `valid_till_ms` bigint NOT NULL DEFAULT '0',
  `status` int NOT NULL DEFAULT '0',
  `created_at` datetime ,
  `updated_at` datetime ,
  PRIMARY KEY (`id`)
)