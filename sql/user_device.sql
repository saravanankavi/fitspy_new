CREATE TABLE `user_device` (
  `id` int NOT NULL AUTO_INCREMENT,
  `device_id` varchar(100) DEFAULT NULL,
  `source` enum('android','ios','web','na') DEFAULT 'na',
  `app` varchar(255) DEFAULT NULL,
  `version` text,
  `id_user` int DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `os_version` varchar(10) DEFAULT NULL,
  `fcm_token` text,
  `status` int NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))