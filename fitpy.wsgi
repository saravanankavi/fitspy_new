#!/usr/bin/python3
#import sys
#sys.path.insert(0, '/var/www/html/fitpy')

#from app import app as application
import sys
import logging

sys.path.insert(0, '/var/www/html/fitpy')
sys.path.insert(0, '/var/www/html/fitpy/flaskvenv/lib/python3.10/site-packages/')

# Set up logging
logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

# Import and run the Flask app
from app import app as application
#####
#import sys
#sys.path.insert(0, '/var/www/html/fitpy')  # Add the directory containing your application to the sys.path

#from fitpy import app  # Replace 'your_application' with the actual name of your application

# If your application requires any additional setup or configuration,
# you can do it here before the application is created.

#application = app  # Create the WSGI application using your application object

