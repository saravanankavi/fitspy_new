from flask import Flask
import socket
import json
from class_functions.api_funtion import ApiData
from flask_socketio import SocketIO

app=Flask(__name__)
app.config['SECRET_KEY'] = 'your_secret_key'
socketio = SocketIO(app)
value = ApiData()
@app.route('/',methods=['POST','GET'])
def home():     
    data = {
        'msg':'Welcome to Home Screeen'
    }
    datas = json.dumps(data)
    return datas

@app.route('/profile',methods=['POST','GET'])
def profile_page():
    return value.profile()

@app.route('/signin',methods=['POST','GET'])
def signin():
    return value.sign_in()

@app.route('/signup',methods=['POST','GET'])
def signup():
    return value.sign_up()


@app.route('/otp',methods=['POST','GET'])
def otp_valid():
    return value.otp_page()

@app.route('/dashboard',methods=['POST','GET'])
def dashboard():
    return value.home_page()

@app.route('/activity',methods=['POST','GET'])
def activity():
    return value.activity_page()

@socketio.on('connect')
def handle_connect():
    print('Client connected')
    return 'Client connected'

@socketio.on('disconnect')
def handle_disconnect():
    print('Client disconnected')
    return 'Client disconnected'

@socketio.on('message')
def handle_message(data):
    message = data['message']
    socketio.emit('message', {'message': message})
    print(data)
    return message


if __name__ == '__main__':
    socketio.run(app,host='0.0.0.0', port=81, debug=True)
